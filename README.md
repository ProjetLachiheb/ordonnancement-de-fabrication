# Ordonnancement de fabrication

Le but de ce projet est l’ordonnancement de n taches sur un atelier constitué de 3 machines A, B et C. Nous devons rendre une permutation P qui minimise la date à laquelle les n taches sont passées par les 3 machines.