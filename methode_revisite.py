#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import heapq
import math
import time


 
class Noeud :
    """ Instanciation d'un Noeud :"""
    # Afficha des permutation
    def __init__(self, probleme, id, successeur=None, pere=None):
        self.BI = 0
        self.id = id
        self.P = probleme
        if successeur == None :
            self.successeur = self.init_successeur()
        else :
            self.successeur = successeur
        self.pere = pere
        if pere == None :
            self.permutation = [id]
        else:
            self.permutation = []
            self.permutation.extend(pere.permutation)
            self.permutation.append(id)  
    
    def __lt__(self, other):
        """ Utile pour le chois d'un noeud ayant la même valeur de Borne inferieur :"""
        return len(self.successeur) < len(other.successeur)
    
    def __str__(self) :
        """ Chaine de caractere du Noeud :"""
        l = [str(x) for x in self.permutation]
        return " Tache " + "\t["+",".join(l)+"]" + "\tsuccesseur=" + str(self.successeur)
    
    def init_successeur(self) :
        """ Initialisation des successeurs lors de la creation :"""
        liste = [x+1 for x in range(self.P.mat.shape[1])]
        liste.remove(self.id)
        return liste
    
    def expand(self) :
        """ Extension des noeuds successeur du noeud :"""
        nouveauNoeud = []
        for i in self.successeur :
            list_intermediaire = list (self.successeur)
            list_intermediaire.remove(i)
            nouveauNoeud.append(Noeud(self.P,i,list_intermediaire,self))
        return nouveauNoeud


# Construction du graphe :
class Probleme :
    """ On commence l'algorithme avec aucune indication sur la Borne inférieur """
    def __init__(self, matrice, borne_i) :
        self.mat = matrice
        self.borne = borne_i            # Choix de la borne utilisee
        self.borneSup = math.inf        # Borne superieur
        self.borneInf = None            # Calcul la borne Inferieur au depart
        self.solution = None            # Solution final
        self.nb_noeuds_visites = 0      # nombre de noeuds visites
        self.reserve = [] 
        
    def transformation_matrice_liste (self,mat) :
        """ Transformation d'une matrice en une liste de tache :"""
        l_taches = []
        m = np.vstack((mat[0,:], mat[2,:]))     # 3 machines fixes
        for i in range (mat.shape[1]) :         # parcours les taches
            l_taches.append((i+1,'A',m[0][i]))
            l_taches.append((i+1,'C',m[1][i]))
        return l_taches

    def algorithme_Johnson (self,X) :
        """ Ordonnancement de Johnson :"""
        G = []
        D = []
        while len(X) != 0 :
            dmin = min(X, key = lambda t: t[2])
            if dmin[1] == 'A' :
                G.append(dmin[0])  
            else :
                D.insert(0, dmin[0])
            # Recupere les taches à supprimer :
            l_supp = list(filter(lambda x: x[0] == dmin[0], X))
            # On les supprimes :
            for i in l_supp :
                X.remove(i)
        return G+D
        
        
    def solution_au_depart (self) :
        """ Calcul l'optimum de la solution de depart avec Johnson, mais non utilisé dans algorithme :"""
        l_taches = self.transformation_matrice_liste(self.mat)
        #print(l_taches)
        permutation_opt = self.algorithme_Johnson (l_taches)
        #print(permutation_opt)
        opt = self.borneX(permutation_opt[0:-1]) 
        return opt#, permutation_opt
    
    def duree_necessaire_machine (self, l_PI) :
        """ Retourne necessaire d'execution des taches pour la machine A,B et C :"""
        t_pi_A = self.mat[0][l_PI[0]-1]
        t_pi_B = t_pi_A + self.mat[1][l_PI[0]-1]
        t_pi_C = t_pi_B + self.mat[2][l_PI[0]-1]
        for i in l_PI[1:] :
            t_pi_A += self.mat[0][i-1]
            if t_pi_A > t_pi_B :
                diff = t_pi_A - t_pi_B
                t_pi_B += diff + self.mat[1][i-1]
            else :
                t_pi_B += self.mat[1][i-1]
            if t_pi_B > t_pi_C :
                diff = t_pi_B - t_pi_C
                t_pi_C += diff + self.mat[2][i-1]
            else :
                t_pi_C += self.mat[2][i-1]
        return t_pi_A, t_pi_B, t_pi_C
    
    def duree_necessaire_machine_AB (self, l_PI) :
        """ Retourne necessaire d'execution des taches pour la machine A et B :"""        
        t_pi_A = self.mat[0][l_PI[0]-1]
        t_pi_B = t_pi_A + self.mat[1][l_PI[0]-1]
        for i in l_PI[1:] :
            t_pi_A += self.mat[0][i-1]
            if t_pi_A > t_pi_B :
                diff = t_pi_A - t_pi_B
                t_pi_B += diff + self.mat[1][i-1]
            else :
                t_pi_B += self.mat[1][i-1]
        return t_pi_B
    
    def borneX (self, l_PI) :
        """ Retourne opt en fonction des bornes choisit dans le probleme :""" 
        if self.borne == 1 :
            return self.borneB1(l_PI)
        elif self.borne == 2 :
            return max(self.borneB1(l_PI), self.borneB2(l_PI), self.borneB3(l_PI))
        else :
            return self.borneB3(l_PI)
    
    def borneB1 (self, l_PI) : #(l_PI) : le debut de la permutation des taches ex: 1, 2
        """ Retourne la valeur de la borne 1 :"""
        b_A_pi = b_B_pi = b_C_pi = 0
        # Calcul de pi_A/B/C (machine A/B/C)
        t_A_pi, t_B_pi, t_C_pi = self.duree_necessaire_machine (l_PI)
        pi_prime = [i for i in range(self.mat.shape[1]) if i+1 not in l_PI] # liste de PI'
        min_a=min_bc=min_c= min_b= min_ab=math.inf
        for t in pi_prime :
            b_A_pi += self.mat[0][t] # som_Di_A
            b_B_pi += self.mat[1][t] # som_Di_B
            b_C_pi += self.mat[2][t] # som_Di_C
            if self.mat[0][t] < min_a: # min_A
                min_a = self.mat[0][t]
            if self.mat[1][t] < min_b : # min_B
                min_b = self.mat[1][t]
            if self.mat[0][t] + self.mat[1][t] < min_ab : #min(dA+dB)
                min_ab = self.mat[0][t] + self.mat[1][t]
            if self.mat[1][t] + self.mat[2][t] < min_bc : # min(dB+dC)
                min_bc = self.mat[1][t] + self.mat[2][t]
            if self.mat[2][t] < min_c : # min_C
                min_c = self.mat[2][t]
        b_A_pi += t_A_pi + min_bc
        b_B_pi += max(t_B_pi, t_A_pi+min_a) + min_c
        b_C_pi += max(t_C_pi, t_B_pi + min_b, t_A_pi + min_ab)
        return max (b_A_pi, b_B_pi, b_C_pi)
    
    def borneB2 (self, l_PI) :
        """ Retourne la valeur de la borne 2 :"""
        t_pi_A = 0
        pi_prime = [i for i in range(self.mat.shape[1]) if i+1 not in l_PI] # liste de PI'
        l_max = [0]*(len(pi_prime))                 #Creation d'une liste de taille PI`
        for tache in l_PI :                         #Parcours des taches dans PI
            t_pi_A += self.mat[0][tache-1]          #Machine A sur la tache l_PI
        for j,tache in enumerate (pi_prime) :                     #Parcours des taches dans PI' 
            l_max[j] += t_pi_A
            l_max[j] += self.mat[0][tache] + self.mat[1][tache] + self.mat[2][tache]
            l_max[j] += sum([self.mat[0][i] for i in pi_prime if tache != i and self.mat[0][i]<=self.mat[2][i]])
            l_max[j] += sum([self.mat[2][i] for i in pi_prime if tache != i and self.mat[0][i]>self.mat[2][i]])
        return max(l_max)
    
    def borneB3 (self, l_PI) :
        """ Retourne la valeur de la borne 3 :"""
        pi_prime = [i for i in range(self.mat.shape[1]) if i+1 not in l_PI] # liste de PI'
        l_max = [0]*(len(pi_prime))                 #Creation d'une liste de taille PI`
        t_pi_B = self.duree_necessaire_machine_AB (l_PI)
        for j,tache in enumerate(pi_prime) :                     #Parcours des taches dans PI' 
            l_max[j] += t_pi_B
            l_max[j] += self.mat[1][tache] + self.mat[2][tache]
            l_max[j] += sum([self.mat[1][i] for i in pi_prime if tache != i and self.mat[1][i]<=self.mat[2][i]])
            l_max[j] += sum([self.mat[2][i] for i in pi_prime if tache != i and self.mat[1][i]>self.mat[2][i]])
        return max(l_max)        

                
    def dessine_matrice (self):
        """ Affichage de la matrice :"""
        for i in range (self.mat.shape[0]) :
            print(self.mat[i])
            
    
class Matrice :
    """ Construction d'une matrice """
        # Instance :
    def donnee_non_correles(self,n) :
        """ Retourne une matrice de donnee non correles :"""
        mat = np.zeros((3, n), int)
        for i in range (3) : # 3 machines
            for j in range (n) : # 3 taches
                mat[i][j] = int(np.random.uniform(1, 100, 1)[0])
        return mat # liste de tableau

    def correlation_duree(self,n) :
        """ Retourne une matrice de donnee correles :"""
        mat = np.zeros((3, n), int)
        for j in range (n) :
            entier = np.random.randint(0, 4)
            for i in range (3) :
                mat[i][j] = int(np.random.uniform(20*entier, 20*entier + 20, 1)[0])
        return mat

    def correlation_machine(self,n) :
        """ Retourne une matrice de donnee correles sur machine :"""
        mat = np.zeros((3, n), int)
        for i in range (3) :
            for j in range (n) :
                mat[i][j] = int(np.random.uniform(15*((i+1)-1)+1, 15*((i+1)-1) + 100, 1)[0])
        return mat    

######################################################################################        
""" Choix de la matrice :""" 

#mat = np.loadtxt('opt_62.txt', dtype=int,skiprows=1)
#mat = np.loadtxt('opt_282.txt', dtype=int,skiprows=1)
#mat = np.loadtxt('opt_357.txt', dtype=int,skiprows=1)

mat = np.array([[26, 77 , 4, 79, 28, 68, 87, 56, 19, 56, 37, 49],
[64, 50, 89 ,62 ,88 ,15 ,83 ,15 ,78 ,58 ,88 ,69],
[32 ,37, 99, 31, 82 ,68 ,42 ,48 ,58 ,77,  9, 36]])
#mat = np.array([[81, 12, 92, 43, 82, 45, 17, 49, 27, 17, 87, 15, 80, 75 ,40 ,95 ,93 ,24 ,96, 88, 56, 22, 41, 83, 31, 29, 75, 15, 81, 29],
#    [97, 24, 90 ,57 ,11 ,35, 94, 76, 21, 24 , 5 ,39, 78, 15, 16, 61, 25, 89, 58, 49, 58, 93, 32, 12, 85,98 ,88, 32, 17, 48],
#    [47, 25, 68, 25, 39, 41, 96,  5, 14, 36, 14, 13, 84 ,99, 67 , 8 ,96 ,32 ,38, 45,  2 ,58, 94, 52, 90,24,  4, 17, 67,  5]])
#M = Matrice()
#mat = M.correlation_duree(8)

print("---------------------------------------------")

""" Initialisation du probleme :"""
P = Probleme(mat,1)
tmp = [(P.borneX([i+1]), Noeud(P,i+1,None,None)) for i in range (mat.shape[1])]    
frontiere = []
for i in tmp :
     heapq.heappush(frontiere,i)

""" Affichage de la matrice du problème : """
print("\nAffichage de la matrice :")
P.dessine_matrice()

""" Debut de l'algorithme : """
startTime = time.time()
while frontiere != [] :
    #print("--------------")
    #for i in frontiere :
    #    print(i[0],i[1]) # Si tous le meme binf plus petit successeur
    (Binf, bestNoeud) = heapq.heappop(frontiere)
    bestNoeud.binf = Binf
    bestNoeud.P.nb_noeuds_visites += 1
    #print("\nSelectionne : {},{}".format(Binf,bestNoeud))
    # Mettre a jour la Borne inferieur :
    if Binf >= P.borneSup :
        break
    if len(bestNoeud.successeur) == 1 and Binf < P.borneSup:
        #print("MAJ de borne SUP ", Binf)
        P.borneSup = Binf
        P.solution = (Binf,bestNoeud)
        continue
    if Binf < P.borneSup and len(bestNoeud.successeur) > 1: # no end
        P.borneInf = Binf
  
    nouveauxNoeuds = bestNoeud.expand()
    for n in nouveauxNoeuds:
        Binf = bestNoeud.P.borneX(n.permutation)
        heapq.heappush(frontiere, (Binf,n))
stopTime = time.time() 

""" Affichage d'information sur la solution de l'algorithlme :"""

print("\nSolution optimale :",P.solution[0], P.solution[1])
print("\nTemps ecoules : ",stopTime-startTime)
print("\nNombre de noeuds visités :",P.nb_noeuds_visites)



















"""
while frontiere != [] :
    print("--------------")
    for i in frontiere :
        print(i[0],i[1]) # Si tous le meme binf plus petit successeur
    (Binf, bestNoeud) = heapq.heappop(frontiere)
    bestNoeud.binf = Binf
    bestNoeud.P.nb_noeuds_visites += 1
    print("\nSelectionne : {},{}".format(Binf,bestNoeud))
    # Mettre a jour la Borne inferieur :
    if Binf >= P.borneSup :
        break
    if len(bestNoeud.successeur) == 1 and Binf < P.borneSup:
        print("MAJ de borne SUP ", Binf)
        P.borneSup = Binf
        P.solution = (Binf,bestNoeud)
        continue
    if Binf < P.borneSup and len(bestNoeud.successeur) > 1: # no end
        P.borneInf = Binf
  
    nouveauxNoeuds = bestNoeud.expand()
    for n in nouveauxNoeuds:
        Binf = bestNoeud.P.borneX(n.permutation)
        heapq.heappush(frontiere, (Binf,n))
"""


"""
P = Probleme(mat,1)
#frontiere = [(P.borneX([i+1]), Noeud(P,i+1,None,None)) for i in range (mat.shape[1])]    

frontiere = Q.PriorityQueue()

for i in range (mat.shape[1]) :
    noeud = Noeud(P,i+1, None,None)
    Binf = P.borneX([i+1])
    noeud.BI = Binf
    frontiere.put(noeud)

startTime = time.time()
while not frontiere.empty() :
    noeud = frontiere.get()
    #print("Selection : ", noeud)    
    noeud.P.nb_noeuds_visites += 1
    if noeud.BI >=P.borneSup :
        break
    if len(noeud.successeur) == 1 and noeud.BI < P.borneSup:
        #print("MAJ de borne SUP ", noeud.BI)
        P.borneSup = noeud.BI
        P.solution = (noeud.BI,noeud)
        continue
    if noeud.BI < P.borneSup and len(noeud.successeur) > 1: # no end
        P.borneInf = noeud.BI
    nouveauxNoeuds = noeud.expand()
    for n in nouveauxNoeuds:
        Binf = n.P.borneX(n.permutation)
        n.BI = Binf
        frontiere.put(n)
stopTime = time.time()    

print("\n",P.solution[0], P.solution[1])
print("\n",stopTime-startTime)
print("\nNombre de noeuds visités : ",P.nb_noeuds_visites)
"""

""" POUR STOPPER AU BOUT D'UN CERTAIN TEMPS


nb_iteration = 10
TEMPS = 420
while (i < nb_iteration) :

    M = Matrice()
    mat = M.donnee_non_correles(50)
    #mat = M.correlation_duree(14)                                                                                                          
    #mat = M.correlation_machine(75)                                                                                                        
    #mat = np.loadtxt('opt_62.txt', dtype=int,skiprows=1)                                                                                   

    P = Probleme(mat,1)
    tmp = [(P.borneX([i+1]), Noeud(P,i+1,None,None)) for i in range (mat.shape[1])]
    frontiere = []
    for i in tmp :
        heapq.heappush(frontiere,i)
    #print("----------------------------------------------------")                                                                          
    #############                                                                                                                           
    l1.append(P.tmp)
    startTime = time.time()
    while frontiere != [] :
        #print("--------------")                                                                                                            
        #for i in frontiere :                                                                                                               
        #    print(i[0],i[1]) # Si tous le meme binf plus petit successeur                                                                  
        (Binf, bestNoeud) = heapq.heappop(frontiere)
        bestNoeud.binf = Binf
        bestNoeud.P.nb_noeuds_visites += 1
        #print("\nSelectionne : {},{}".format(Binf,bestNoeud))                                                                              
        # Mettre a jour la Borne inferieur :                                                                                                
        if Binf >= P.borneSup :
            break
        if len(bestNoeud.successeur) == 1 and Binf < P.borneSup:
            #print("MAJ de borne SUP ", Binf)                                                                                               
            P.borneSup = Binf
            P.solution = (Binf,bestNoeud)
            continue
        if Binf < P.borneSup and len(bestNoeud.successeur) > 1: # no end                                                                    
            P.borneInf = Binf
        nouveauxNoeuds = bestNoeud.expand()
        for n in nouveauxNoeuds:
            Binf = bestNoeud.P.borneX(n.permutation)
            heapq.heappush(frontiere, (Binf,n))
        stop1 = time.time()-startTime
        if stop1 > TEMPS :
            nb_bloque1.append((P.tmp,Binf))
            break
    stopTime = time.time()

    l_f1.append((stopTime-startTime,P.nb_noeuds_visites))
#    l2.append(P.solution[0])#, P.solution[1])                                                                                              
    #print("\nTemps ecoules : ",stopTime-startTime)                                                                                         
    #print("\nNombre de noeuds visités :",P.nb_noeuds_visites)                                                                              

    P = Probleme(mat,2)
    tmp = [(P.borneX([i+1]), Noeud(P,i+1,None,None)) for i in range (mat.shape[1])]
    frontiere = []
    for i in tmp :
        heapq.heappush(frontiere,i)
    #print("------------------------------------------------------")                                                                        
    startTime = time.time()
    while frontiere != [] :
        #print("--------------")                                                                                                            
        #for i in frontiere :                                                                                                               
        #    print(i[0],i[1]) # Si tous le meme binf plus petit successeur                                                                  
        (Binf, bestNoeud) = heapq.heappop(frontiere)
        bestNoeud.binf = Binf
        bestNoeud.P.nb_noeuds_visites += 1
        #print("\nSelectionne : {},{}".format(Binf,bestNoeud))                                                                              
        # Mettre a jour la Borne inferieur :                                                                                                
        if Binf >= P.borneSup :
            break
        if len(bestNoeud.successeur) == 1 and Binf < P.borneSup:
            #print("MAJ de borne SUP ", Binf)                                                                                               
            P.borneSup = Binf
            P.solution = (Binf,bestNoeud)
            continue
        if Binf < P.borneSup and len(bestNoeud.successeur) > 1: # no end                                                                    
            P.borneInf = Binf
        nouveauxNoeuds = bestNoeud.expand()
        for n in nouveauxNoeuds:
            Binf = bestNoeud.P.borneX(n.permutation)
            heapq.heappush(frontiere, (Binf,n))
        stop2 = time.time()-startTime
        if stop2 > TEMPS :
            nb_bloque2.append((P.tmp,Binf))
            break

    stopTime = time.time()
    l_f2.append((stopTime-startTime,P.nb_noeuds_visites))
    l = []
    for x, y in zip(l_f1, l_f2) :
        l.append((x,y))

    temps = np.zeros((len(l_f1),2))
    noeuds = np.zeros((len(l_f1),2))
    for i in range (len(l_f1)) :
        for j in range (2) :
            if j == 0 :
                temps[i][j] = (l_f1[i][0])
                noeuds[i][j] = (l_f1[i][1])
            else :
                temps[i][j] = (l_f2[i][0])
                noeuds[i][j] = (l_f2[i][1])

    i+=1


"""


