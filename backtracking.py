#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import heapq
import math
import time


 
class Noeud :
    """ Instanciation d'un Noeud :"""
    def __init__(self, probleme, id, successeur=None, pere=None):
        self.id = id
        self.P = probleme
        if successeur == None :
            self.successeur = self.init_successeur()
        else :
            self.successeur = successeur
        self.pere = pere
        if pere == None :
            self.permutation = [id]
        else:
            self.permutation = []
            self.permutation.extend(pere.permutation)
            self.permutation.append(id)
            
    def __lt__(self, other):
        return str(self) < str(other)    
        
    def __str__(self) :
        """ Chaine de caractere du Noeud :"""
        l = [str(x) for x in self.permutation]
        return "Tache " + "\t["+",".join(l)+"]" + "\tsuccesseur=" + str(self.successeur)
    
    def init_successeur(self) :
        """ Initialisation des successeurs lors de la creation :"""
        liste = [x+1 for x in range(self.P.mat.shape[1])]
        liste.remove(self.id)
        return liste
    
    def expand(self) :
        """ Extension des noeuds successeur du noeud :"""
        nouveauNoeud = []
        for i in self.successeur :
            list_intermediaire = list (self.successeur)
            list_intermediaire.remove(i)
            nouveauNoeud.append(Noeud(self.P,i,list_intermediaire,self))
        return nouveauNoeud

# Construction du graphe :
class Probleme :
    """ On commence l'algorithme avec une indication sur la Borne inférieur """
    def __init__(self, matrice, borne_i) :
        self.mat = matrice
        self.borne = borne_i
        self.nb_noeuds_visites = 0
        self.solution, self.permutationD = self.solution_au_depart()
        self.reserve = [] 
        
    def transformation_matrice_liste (self,mat) :
        """ Transformation d'une matrice en une liste de tache :"""
        l_taches = []
        m = np.vstack((mat[0,:], mat[2,:])) # 3 machines fixes
        for i in range (mat.shape[1]) :     # parcours les taches
            l_taches.append((i+1,'A',m[0][i]))
            l_taches.append((i+1,'C',m[1][i]))
        return l_taches

    def algorithme_Johnson (self,X) :
        """ Ordonnancement de Johnson :"""
        G = []
        D = []
        while len(X) != 0 :
            dmin = min(X, key = lambda t: t[2])
            if dmin[1] == 'A' :
                G.append(dmin[0])  
            else :
                D.insert(0, dmin[0])
            # Recupere les taches à supprimer :
            l_supp = list(filter(lambda x: x[0] == dmin[0], X))
            # On les supprimes :
            for i in l_supp :
                X.remove(i)
        return G+D
        
        
    def solution_au_depart (self) :
        """ Calcul l'optimum de la solution de depart avec Johnson :"""
        l_taches = self.transformation_matrice_liste(self.mat)
        #print(l_taches,"\n")
        permutation_opt = self.algorithme_Johnson (l_taches)
        #print(permutation_opt)
        opt = self.borneX(permutation_opt[0:-1]) 
        return opt, permutation_opt
    
    def duree_necessaire_machine (self, l_PI) :
        """ Retourne necessaire d'execution des taches pour la machine A,B et C :"""
        t_pi_A = self.mat[0][l_PI[0]-1]
        t_pi_B = t_pi_A + self.mat[1][l_PI[0]-1]
        t_pi_C = t_pi_B + self.mat[2][l_PI[0]-1]
        for i in l_PI[1:] :
            t_pi_A += self.mat[0][i-1]
            if t_pi_A > t_pi_B :
                diff = t_pi_A - t_pi_B
                t_pi_B += diff + self.mat[1][i-1]
            else :
                t_pi_B += self.mat[1][i-1]
            if t_pi_B > t_pi_C :
                diff = t_pi_B - t_pi_C
                t_pi_C += diff + self.mat[2][i-1]
            else :
                t_pi_C += self.mat[2][i-1]
        return t_pi_A, t_pi_B, t_pi_C
    
    def duree_necessaire_machine_AB (self, l_PI) :
        """ Retourne necessaire d'execution des taches pour la machine A et B :"""
        t_pi_A = self.mat[0][l_PI[0]-1]
        t_pi_B = t_pi_A + self.mat[1][l_PI[0]-1]
        for i in l_PI[1:] :
            t_pi_A += self.mat[0][i-1]
            if t_pi_A > t_pi_B :
                diff = t_pi_A - t_pi_B
                t_pi_B += diff + self.mat[1][i-1]
            else :
                t_pi_B += self.mat[1][i-1]
        return t_pi_B
    
    def borneX (self, l_PI) :
        """ Retourne opt en fonction des bornes choisit dans le probleme :"""
        if self.borne == 1 :
            return self.borneB1(l_PI)
        elif self.borne == 2 :
            return max(self.borneB1(l_PI), self.borneB2(l_PI), self.borneB3(l_PI))
        else :
            return self.borneB3(l_PI)
    
    def borneB1 (self, l_PI) : #(l_PI) : le debut de la permutation des taches ex: 1, 2
        """ Retourne la valeur de la borne 1 :"""
        b_A_pi = b_B_pi = b_C_pi = 0
        # Calcul de pi_A/B/C (machine A/B/C)
        t_A_pi, t_B_pi, t_C_pi = self.duree_necessaire_machine (l_PI)
        pi_prime = [i for i in range(self.mat.shape[1]) if i+1 not in l_PI] # liste de PI'
        min_a=min_bc=min_c= min_b= min_ab=math.inf
        for t in pi_prime :
            b_A_pi += self.mat[0][t] # som_Di_A
            b_B_pi += self.mat[1][t] # som_Di_B
            b_C_pi += self.mat[2][t] # som_Di_C
            if self.mat[0][t] < min_a: # min_A
                min_a = self.mat[0][t]
            if self.mat[1][t] < min_b : # min_B
                min_b = self.mat[1][t]
            if self.mat[0][t] + self.mat[1][t] < min_ab : #min(dA+dB)
                min_ab = self.mat[0][t] + self.mat[1][t]
            if self.mat[1][t] + self.mat[2][t] < min_bc : # min(dB+dC)
                min_bc = self.mat[1][t] + self.mat[2][t]
            if self.mat[2][t] < min_c : # min_C
                min_c = self.mat[2][t]
        b_A_pi += t_A_pi + min_bc
        b_B_pi += max(t_B_pi, t_A_pi+min_a) + min_c
        b_C_pi += max(t_C_pi, t_B_pi + min_b, t_A_pi + min_ab)
        return max (b_A_pi, b_B_pi, b_C_pi)
    
    def borneB2 (self, l_PI) :
        """ Retourne la valeur de la borne 2 :"""
        t_pi_A = 0
        pi_prime = [i for i in range(self.mat.shape[1]) if i+1 not in l_PI] # liste de PI'
        l_max = [0]*(len(pi_prime))                 #Creation d'une liste de taille PI`
        for tache in l_PI :                         #Parcours des taches dans PI
            t_pi_A += self.mat[0][tache-1]          #Machine A sur la tache l_PI
        for j,tache in enumerate (pi_prime) :                     #Parcours des taches dans PI' 
            l_max[j] += t_pi_A
            l_max[j] += self.mat[0][tache] + self.mat[1][tache] + self.mat[2][tache]
            l_max[j] += sum([self.mat[0][i] for i in pi_prime if tache != i and self.mat[0][i]<=self.mat[2][i]])
            l_max[j] += sum([self.mat[2][i] for i in pi_prime if tache != i and self.mat[0][i]>self.mat[2][i]])
        return max(l_max)
    
    def borneB3 (self, l_PI) :
        """ Retourne la valeur de la borne 3 :"""
        pi_prime = [i for i in range(self.mat.shape[1]) if i+1 not in l_PI] # liste de PI'
        l_max = [0]*(len(pi_prime))                 #Creation d'une liste de taille PI`
        t_pi_B = self.duree_necessaire_machine_AB (l_PI)
        for j,tache in enumerate(pi_prime) :                     #Parcours des taches dans PI' 
            l_max[j] += t_pi_B
            l_max[j] += self.mat[1][tache] + self.mat[2][tache]
            l_max[j] += sum([self.mat[1][i] for i in pi_prime if tache != i and self.mat[1][i]<=self.mat[2][i]])
            l_max[j] += sum([self.mat[2][i] for i in pi_prime if tache != i and self.mat[1][i]>self.mat[2][i]])
        return max(l_max)        

                
    def dessine_matrice (self):
        """ Affichage de la matrice :"""
        for i in range (self.mat.shape[0]) :
            print(self.mat[i])
    

class Matrice :
    """ Construction d'une matrice """
    def donnee_non_correles(self,n) :
        """ Retourne une matrice de donnee non correles :"""
        mat = np.zeros((3, n), int)
        for i in range (3) : # 3 machines
            for j in range (n) : # 3 taches
                mat[i][j] = int(np.random.uniform(1, 100, 1)[0])
        return mat # liste de tableau

    def correlation_duree(self,n) :
        """ Retourne une matrice de donnee correles :"""
        mat = np.zeros((3, n), int)
        for j in range (n) :
            entier = np.random.randint(0, 4)
            for i in range (3) :
                mat[i][j] = int(np.random.uniform(20*entier, 20*entier + 20, 1)[0])
        return mat

    def correlation_machine(self,n) :
        """ Retourne une matrice de donnee correles sur machine :"""
        mat = np.zeros((3, n), int)
        for i in range (3) :
            for j in range (n) :
                mat[i][j] = int(np.random.uniform(15*((i+1)-1)+1, 15*((i+1)-1) + 100, 1)[0])
        return mat    
######################################################################################        

def backtracking (frontiere) :
    #print("------------------")
    #for i in frontiere :
    #    print(i[0], i[1])
    while frontiere != [] :
        #for i in frontiere :
        #    print(i[0], i[1])
        (Binf, bestNoeud) = heapq.heappop(frontiere)
        #print(Binf)
        bestNoeud.P.nb_noeuds_visites += 1
        # Condition d'arret :
        if len(bestNoeud.successeur) == 1 :
            if Binf < bestNoeud.P.solution : # MAJ celon condition :
                #print("==> MAJ solution {} en {} ".format(P.solution,Binf))
                bestNoeud.P.solution = Binf
                if len(bestNoeud.P.reserve) > 0 :
                    bestNoeud.P.reserve = []
                bestNoeud.P.reserve.append((Binf, bestNoeud))
                continue
            if Binf == bestNoeud.P.solution and len(bestNoeud.P.reserve) == 0: # ou prendre valeur Johnson ?
                #print("==> MAJ solution {} en {} (Car non presence d'une permutation)".format(P.solution,Binf))
                bestNoeud.P.reserve.append((Binf, bestNoeud))
                continue
        elif Binf > bestNoeud.P.solution :
            #print("x {} pas interessant".format(bestNoeud))
            continue
        else :
            nouveauxNoeuds = bestNoeud.expand()
            frontiere_bis = []
            for n in nouveauxNoeuds:
                Binf = bestNoeud.P.borneX(n.permutation)
                heapq.heappush(frontiere_bis, (Binf,n))
            backtracking(frontiere_bis)
    #print("------------------")


""" Choix de la matrice :""" 
mat = np.array([[13, 7, 26, 2], [3, 12, 9, 6], [12, 16, 7, 1]])   
#mat = np.loadtxt('opt_62.txt', dtype=int,skiprows=1)
#mat = np.loadtxt('opt_282.txt', dtype=int,skiprows=1)
#mat = np.loadtxt('opt_357.txt', dtype=int,skiprows=1)
#mat = np.array([[26, 77 , 4, 79, 28, 68, 87, 56, 19, 56, 37, 49],
#[64, 50, 89 ,62 ,88 ,15 ,83 ,15 ,78 ,58 ,88 ,69],
#[32 ,37, 99, 31, 82 ,68 ,42 ,48 ,58 ,77,  9, 36]])
#for i in frontiere :
#    print(i[0], i[1])

#M = Matrice()
#mat = M.donnee_non_correles(12)

print("---------------------------------------------")

""" Initialisation du probleme :"""
P1 = Probleme(mat,1)
frontiere = [(P1.borneX([i+1]), Noeud(P1,i+1,None,None)) for i in range (mat.shape[1])] # nombre de tache  

""" Affichage de la matrice du problème : """
print("\nAffichage de la matrice :")
P1.dessine_matrice()

print("\nSolution de depart :", P1.solution)


""" Debut de l'algorithme : """
startTime = time.time()
backtracking(frontiere)
stopTime = time.time()

""" Affiche de l'optmimun de départ : """
print("\nAffichage de l'optimum de depart : ")
print("{} de permutation {}".format(P1.solution, P1.permutationD))

print("\nSolution optimale est :")
print(P1.reserve[0][0], P1.reserve[0][1])

print("\nTemps de calcul : {} secondes".format(stopTime-startTime))

print("\nNombre de noeuds visités :", P1.nb_noeuds_visites)


















"""
Test de Calcul :
    
l_f1 = []
l_f2 = []
for k in range (15) :  
    #M = Matrice()
    #mat = M.donnee_non_correles(3)
    
    P1 = Probleme(mat,1)
    #print("{} de permutation {}".format(P1.solution, P1.permutationD))

    P2 = Probleme(mat,2)                
    #print("{} de permutation {}".format(P2.solution, P2.permutationD))

    
    frontiere = [(P1.borneX([i+1]), Noeud(P1,i+1,None,None)) for i in range (mat.shape[1])] # nombre de tache  
    startTime = time.time()
    backtracking(frontiere)
    stopTime = time.time()
    l_f1.append((stopTime-startTime,P1.nb_noeuds_visites))
    #print(P1.reserve[0][0], P1.reserve[0][1])
    
    frontiere = [(P2.borneX([i+1]), Noeud(P2,i+1,None,None)) for i in range (mat.shape[1])] # nombre de tache  
    startTime = time.time()
    backtracking(frontiere)
    stopTime = time.time()
    l_f2.append((stopTime-startTime,P2.nb_noeuds_visites))
    #print(P2.reserve[0][0], P2.reserve[0][1])
   
    l = []
    for x, y in zip(l_f1, l_f2) :
        l.append((x,y))
        
    temps = np.zeros((len(l_f1),2))   
    noeuds = np.zeros((len(l_f1),2))
    for i in range (len(l_f1)) :
        for j in range (2) :
            if j == 0 :
                temps[i][j] = (l_f1[i][0])
                noeuds[i][j] = (l_f1[i][1])
            else :
                temps[i][j] = (l_f2[i][0])
                noeuds[i][j] = (l_f2[i][1])
                
print("\nBORNE 1 :")    
print(l_f1)

print("\nBORNE 2 :")
print(l_f2)

print("BORNE1 ET BORNE2 :")
print(l)

print("\n TEMPS :")
print(temps)
print(np.mean(temps,0))

print("\n NB NOEUDS :")
print(noeuds)
print(np.mean(noeuds,0))
"""

  
    
    
    
    
    
    
    
    
            






















